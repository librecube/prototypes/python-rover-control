import socket
import threading
import time
import pygame

# Constants
SERVER_IP = '127.0.0.1'
PORT = 8080
WIDTH, HEIGHT = 400, 300
WHITE = (255, 255, 255)
LIGHT_GRAY = (200, 200, 200)
GRAY = (100, 100, 100)
FONT_SIZE = 14
IMAGE_SIZE = (60, 60)


arrow_up_image = None
arrow_down_image = None
arrow_left_image = None
arrow_right_image = None
background_image = None

current_command = None
previous_command = None
current_speed = 10.0
speed_changed = False

pygame.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Rover Control Interface")
font = pygame.font.Font(None, FONT_SIZE)


def load_images():
    global arrow_up_image, arrow_down_image, arrow_left_image, arrow_right_image, background_image
    arrow_up_image = pygame.image.load('image/arrow_up_image.png').convert_alpha()
    arrow_down_image = pygame.image.load('image/arrow_down_image.png').convert_alpha()
    arrow_left_image = pygame.image.load('image/arrow_left_image.png').convert_alpha()
    arrow_right_image = pygame.image.load('image/arrow_right_image.png').convert_alpha()
    background_image = pygame.image.load('image/background.png').convert()

def send_commands(client_socket):
    global current_command, current_speed, speed_changed
    while True:
        if current_command:
            message = f"{current_command} {current_speed}"
            client_socket.send(message.encode('utf-8'))
            time.sleep(0.1)
            speed_changed = False
        else:
            # Stop command send if no movement command is active
            message = f"stop {current_speed}"
            client_socket.send(message.encode('utf-8'))
            time.sleep(0.1)

def draw_interface():
    screen.blit(background_image, (0, 0))

    # Instructions
    instructions = [
        "Use arrow keys to control the robot:",
        "Up Arrow: Move Forward",
        "Down Arrow: Move Backward",
        "Left Arrow: Turn Left",
        "Right Arrow: Turn Right",
        "Use '+' and '-' to adjust speed"
    ]

    instruction_bg = pygame.Surface((WIDTH - 20, HEIGHT - 20))
    instruction_bg.set_alpha(90)
    instruction_bg.fill(GRAY)
    screen.blit(instruction_bg, (10, 10))

    for i, line in enumerate(instructions):
        text = font.render(line, True, LIGHT_GRAY)
        screen.blit(text, (20, 20 + i * (FONT_SIZE + 2)))

    speed_text = font.render(f"Speed: {current_speed}", True, LIGHT_GRAY)
    text_rect = speed_text.get_rect(topright=(WIDTH - 20, 20))
    screen.blit(speed_text, text_rect)

    # Command image position
    image_position = (WIDTH // 2 - IMAGE_SIZE[0] // 2, HEIGHT - IMAGE_SIZE[1] - 40)

    # Active command display
    if current_command == 'move_forward':
        image_to_display = arrow_up_image
    elif current_command == 'move_backward':
        image_to_display = arrow_down_image
    elif current_command == 'turn_left':
        image_to_display = arrow_left_image
    elif current_command == 'turn_right':
        image_to_display = arrow_right_image
    else:
        image_to_display = None

    if image_to_display:
        scaled_image = pygame.transform.scale(image_to_display, IMAGE_SIZE)
        screen.blit(scaled_image, image_position)

    pygame.display.flip()

def handle_keys():
    global current_command, previous_command, current_speed, speed_changed
    keys = pygame.key.get_pressed()

    # Check for movement keys
    if keys[pygame.K_UP]:
        current_command = 'move_forward'
    elif keys[pygame.K_DOWN]:
        current_command = 'move_backward'
    elif keys[pygame.K_LEFT]:
        current_command = 'turn_left'
    elif keys[pygame.K_RIGHT]:
        current_command = 'turn_right'
    else:
        current_command = None

    # Speed will adjust only if a movement command is active
    if current_command:
        if keys[pygame.K_EQUALS] and current_speed < 50.0:  # Max speed limit
            current_speed += 5.0
            speed_changed = True
        elif keys[pygame.K_MINUS] and current_speed > 5.0:  # Min speed limit
            current_speed -= 5.0
            speed_changed = True
    else:
        speed_changed = False  # No speed change if no movement command


def main():
    global current_command, current_speed
    load_images()

    # Client socket set up
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((SERVER_IP, PORT))
    print(
        f"Connected to server at {SERVER_IP}:{PORT}. Use arrow keys to control the robot and '+'/'-' to adjust speed.")

    # Start a thread to continuously send commands
    send_thread = threading.Thread(target=send_commands, args=(client_socket,))
    send_thread.daemon = True
    send_thread.start()

    # Main loop
    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

        handle_keys()
        draw_interface()

        pygame.time.delay(100)

    pygame.quit()
    client_socket.close()


if __name__ == "__main__":
    main()
