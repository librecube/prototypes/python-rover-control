from gui_executor.exec import exec_task
from gui_executor.exec import exec_recurring_task
from gui_executor.exec import StatusType
import requests

from . import SERVER_URL


UI_MODULE_DISPLAY_NAME = "Rover Control Tasks"


@exec_task(display_name="Forward")
def forward(value: int = 1):
    data = {"action": "forward", "value": value}
    result = requests.post(SERVER_URL + "/movement", json=data, timeout=1)
    return result


@exec_task(display_name="Backward")
def Backward(value: int = 1):
    data = {"action": "backward", "value": value}
    result = requests.post(SERVER_URL + "/movement", json=data, timeout=1)
    return result


@exec_task(display_name="Left")
def left(value: int = 1):
    data = {"action": "left", "value": value}
    result = requests.post(SERVER_URL + "/movement", json=data, timeout=1)
    return result


@exec_task(display_name="Right")
def right(value: int = 1):
    data = {"action": "right", "value": value}
    result = requests.post(SERVER_URL + "/movement", json=data, timeout=1)
    return result


@exec_task(display_name="Stop")
def stop(value: int = 0):
    data = {"action": "stop", "value": value}
    result = requests.post(SERVER_URL + "/movement", json=data, timeout=1)
    return result
