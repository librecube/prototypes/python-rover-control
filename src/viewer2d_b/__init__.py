import os
import subprocess


FILE = "main.py"
FILE_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), FILE)

print(FILE_PATH)


def run():
    subprocess.run(["python", FILE_PATH])
