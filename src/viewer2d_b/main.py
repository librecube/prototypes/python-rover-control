import pgzrun

import time
import queue
import math
import json
import threading

from websockets.sync.client import connect


WIDTH = 1024
HEIGHT = 700
FRAMES_PER_SECOND = 30
BACKGROUND_WIDTH = 2971
BACKGROUND_HEIGHT = 2228
SPEED_FACTOR = 0.1

CMD_STOP = 1
CMD_FORWARD = 2
CMD_BACKWARD = 3
CMD_TURN_LEFT = 4
CMD_TURN_RIGHT = 5

rover_position = [0, 0]
rover_angle = 0
rover = Actor("rover")
follow_flag = True
cmd_queue = queue.Queue()
command, args = None, list()


def draw():
    screen.clear()

    if follow_flag:
        background_center = (
            (WIDTH - BACKGROUND_WIDTH) // 2 - rover_position[0],
            (HEIGHT - BACKGROUND_HEIGHT) // 2 + rover_position[1],
        )
        rover.pos = [WIDTH // 2, HEIGHT // 2]
    else:
        background_center = (
            -BACKGROUND_WIDTH // 2 + WIDTH // 2,
            -BACKGROUND_HEIGHT // 2 + HEIGHT // 2,
        )
        rover.pos = [WIDTH // 2 + rover_position[0], HEIGHT // 2 - rover_position[1]]

    rover.angle = rover_angle

    screen.blit("background", background_center)
    screen.draw.text(
        f"pos=({rover_position[0]:.2f} ,{rover_position[0]:.2f}), angle={rover_angle:.2f} deg",
        (10, 10),
    )

    rover.draw()


def on_mouse_down():
    global follow_flag
    follow_flag = not follow_flag


def move_rover(speed):
    global rover_position
    rover_position[0] = (
        rover_position[0] + math.cos(math.radians(rover_angle)) * speed * SPEED_FACTOR
    )
    rover_position[1] = (
        rover_position[1] + math.sin(math.radians(rover_angle)) * speed * SPEED_FACTOR
    )


def rotate_rover(speed):
    global rover_angle
    rover_angle += speed * SPEED_FACTOR


def update():
    global command, args

    if not cmd_queue.empty():
        command, args = cmd_queue.get()
        print("new:", command)

    if command:
        if command == CMD_STOP:
            pass
        else:
            speed = args[0]
            if command == CMD_FORWARD:
                move_rover(speed)
            elif command == CMD_BACKWARD:
                move_rover(-speed)
            elif command == CMD_TURN_LEFT:
                rotate_rover(speed)
            elif command == CMD_TURN_RIGHT:
                rotate_rover(-speed)

    time.sleep(0.1)


def thread1():
    # # read from command line
    # while True:
    #     text = input("command, args: ")
    #     cmd, *args = text.split(",")
    #     cmd = int(cmd)
    #     args = [int(x) for x in args]
    #     cmd_queue.put([cmd, args])

    uri = "ws://localhost:8000/ws/turtle"
    with connect(uri) as websocket:
        while True:
            message = json.loads(websocket.recv())

            if message["action"] == "stop":
                cmd_queue.put((CMD_STOP, []))
            elif message["action"] == "forward":
                speed = message["value"]
                cmd_queue.put((CMD_FORWARD, [speed]))
            elif message["action"] == "backward":
                speed = message["value"]
                cmd_queue.put((CMD_BACKWARD, [speed]))
            elif message["action"] == "left":
                speed = message["value"]
                cmd_queue.put((CMD_TURN_LEFT, [speed]))
            elif message["action"] == "right":
                speed = message["value"]
                cmd_queue.put((CMD_TURN_RIGHT, [speed]))


thread = threading.Thread(target=thread1)
thread.daemon = True
thread.start()

pgzrun.go()
