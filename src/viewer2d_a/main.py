import os
import turtle
import threading
import asyncio

import websockets
from pydantic import BaseModel


BACKGROUND_IMAGE_FILE = "images/background.png"
BACKGROUND_IMAGE_PATH = os.path.join(
    os.path.dirname(os.path.realpath(__file__)), BACKGROUND_IMAGE_FILE
)


# Define the Movement class
class Movement(BaseModel):
    action: str
    value: int = 1


# Shared turtle object for thread safety
shared_turtle = turtle.Turtle()
coord_turtle = turtle.Turtle()

# Flags to control the continuous movement and turning
continue_moving = False
turning = False
current_speed = 1
current_direction = "forward"


# Function to move the turtle based on the command
def move_turtle(command):
    global turning, continue_moving, current_speed, current_direction

    if command.action == "forward":
        shared_turtle.forward(command.value)
    elif command.action == "backward":
        shared_turtle.backward(command.value)
    elif command.action == "left":
        continue_moving = False
        turning = True
        current_direction = "left"
    elif command.action == "right":
        continue_moving = False
        turning = True
        current_direction = "right"
    elif command.action == "stop":
        continue_moving = False
        turning = False
    else:
        print(f"Unknown command: {command.action}")


# Function to update and display current coordinates
def update_coordinates():
    coord_turtle.hideturtle()
    coord_turtle.penup()

    while True:
        x, y = shared_turtle.position()
        angle = shared_turtle.heading()
        coord_turtle.clear()
        coord_turtle.goto(x + 20, y + 20)
        coord_turtle.write(
            f"X: {int(x)}, Y: {int(y)}, Angle: {int(angle)}",
            align="left",
            font=("Arial", 12, "normal"),
        )
        turtle.update()
        turtle.time.sleep(0.1)  # Update frequency


# Function to check and adjust the screen view if the turtle is near the edge
def check_screen_bounds():
    # Initial adjustment on startup to keep the turtle centered
    x, y = shared_turtle.position()
    screen_width = turtle.window_width()
    screen_height = turtle.window_height()
    turtle.setworldcoordinates(
        x - screen_width / 2,
        y - screen_height / 2,
        x + screen_width / 2,
        y + screen_height / 2,
    )

    margin = 50  # Margin from the edge before the screen moves
    target_heading = shared_turtle.setheading(90)

    while True:
        x, y = shared_turtle.position()

        # Check if the turtle is near the edge of the screen and adjust the screen coordinates
        if x > screen_width / 2 - margin:
            turtle.setworldcoordinates(
                x - screen_width / 2 + margin,
                y - screen_height / 2,
                x + screen_width / 2 + margin,
                y + screen_height / 2,
            )
        elif x < -screen_width / 2 + margin:
            turtle.setworldcoordinates(
                x - screen_width / 2 - margin,
                y - screen_height / 2,
                x + screen_width / 2 - margin,
                y + screen_height / 2,
            )

        if y > screen_height / 2 - margin:
            turtle.setworldcoordinates(
                x - screen_width / 2,
                y - screen_height / 2 + margin,
                x + screen_width / 2,
                y + screen_height / 2 + margin,
            )
        elif y < -screen_height / 2 + margin:
            turtle.setworldcoordinates(
                x - screen_width / 2,
                y - screen_height / 2 - margin,
                x + screen_width / 2,
                y + screen_height / 2 - margin,
            )

        turtle.update()
        turtle.time.sleep(0.1)  # Check frequency


async def listen_for_commands():
    global continue_moving, current_speed, current_direction, turning

    uri = "ws://localhost:8000/ws/turtle"
    async with websockets.connect(uri) as websocket:
        while True:
            try:
                message = await websocket.recv()
                command = Movement.parse_raw(message)

                if command.action in ["forward", "backward"]:
                    continue_moving = True
                    turning = False
                    current_speed = command.value
                    current_direction = command.action
                elif command.action in ["left", "right"]:
                    move_turtle(command)
                elif command.action == "stop":
                    move_turtle(command)
                else:
                    print(f"Unknown command: {command.action}")

            except websockets.ConnectionClosed:
                print("Connection closed")
                break


def continuous_movement():
    global continue_moving, current_speed, current_direction, turning

    while True:
        if continue_moving:
            move_turtle(Movement(action=current_direction, value=current_speed))
        elif turning:
            if current_direction == "left":
                shared_turtle.left(1)
            elif current_direction == "right":
                shared_turtle.right(1)
        turtle.update()
        turtle.time.sleep(0.1)


def start_turtle_app():
    global screen

    # Create the turtle window and set initial position
    screen = turtle.Screen()
    screen.setup(width=800, height=600)

    # Set background image
    screen.bgpic(BACKGROUND_IMAGE_PATH)

    screen.tracer(0)  # Turn off automatic screen updates
    shared_turtle.penup()
    shared_turtle.goto(0, 0)
    shared_turtle.setheading(90)
    shared_turtle.pendown()

    # a thread to listen for commands from the WebSocket
    command_thread = threading.Thread(target=asyncio.run, args=(listen_for_commands(),))
    command_thread.daemon = True
    command_thread.start()

    # a thread for continuous movement
    movement_thread = threading.Thread(target=continuous_movement)
    movement_thread.daemon = True
    movement_thread.start()

    # a thread for updating and displaying coordinates
    coord_thread = threading.Thread(target=update_coordinates)
    coord_thread.daemon = True
    coord_thread.start()

    # a thread to check and adjust the screen bounds
    bounds_thread = threading.Thread(target=check_screen_bounds)
    bounds_thread.daemon = True
    bounds_thread.start()

    turtle.done()
