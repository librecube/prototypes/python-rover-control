from controller import Robot, Keyboard
import socket

# Constants
TIME_STEP = 64
MAX_SPEED = 10 # Maximum speed for the wheels
PORT = 8080
SERVER_IP = '127.0.0.1'

# Initialize robot and keyboard
robot = Robot()
keyboard = Keyboard()

# Initialize distance sensors
ds = []
dsNames = ["ds_right", "ds_left"]
for i in range(2):
    ds.append(robot.getDevice(dsNames[i]))
    ds[i].enable(TIME_STEP)

# Initialize wheels
wheels = []
wheelsNames = ["wheel1", "wheel2", "wheel3", "wheel4"]
for i in range(4):
    wheels.append(robot.getDevice(wheelsNames[i]))
    wheels[i].setPosition(float("inf"))
    wheels[i].setVelocity(0.0)

# Enable keyboard
keyboard.enable(TIME_STEP)

# Set up server socket
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind((SERVER_IP, PORT))
server_socket.listen(1)
print(f"Listening on {SERVER_IP}:{PORT}...")

client_socket, addr = server_socket.accept()
print(f"Connection from {addr} has been established.")

# Main control loop
while robot.step(TIME_STEP) != -1:
    data = client_socket.recv(1024).decode('utf-8')
    if not data:
        continue

    # Parse received command and speed
    command, speed = data.split(' ', 1)
    speed = float(speed)  # Speed value

    leftSpeed = 0.0
    rightSpeed = 0.0

    # Set speeds based on received command
    if command == 'move_forward':  # Move forward
        leftSpeed = speed
        rightSpeed = speed
    elif command == 'move_backward':  # Move backward
        leftSpeed = -speed
        rightSpeed = -speed
    elif command == 'turn_left':  # Turn left
        leftSpeed = -speed / 2
        rightSpeed = speed / 2
    elif command == 'turn_right':  # Turn right
        leftSpeed = speed / 2
        rightSpeed = -speed / 2
    elif command == 'stop':  # Stop
        leftSpeed = 0.0
        rightSpeed = 0.0

    # Check distance sensors and avoid obstacles
    for i in range(2):
        if ds[i].getValue() < 950.0:
            # Obstacle detected, turn right
            leftSpeed = MAX_SPEED
            rightSpeed = -MAX_SPEED

    # Set wheel velocities
    wheels[0].setVelocity(leftSpeed)
    wheels[1].setVelocity(rightSpeed)
    wheels[2].setVelocity(leftSpeed)
    wheels[3].setVelocity(rightSpeed)

# Cleanup
keyboard.disable()
client_socket.close()
server_socket.close()
