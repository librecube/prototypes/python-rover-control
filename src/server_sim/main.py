import queue

from fastapi import FastAPI, WebSocket, WebSocketDisconnect
from pydantic import BaseModel
from fastapi.responses import RedirectResponse
import starlette.status as status


app = FastAPI()


class Movement(BaseModel):
    action: str
    value: int = 1


# Create a thread-safe queue to store incoming messages
message_queue = queue.Queue()

connected_clients = set()


@app.websocket("/ws/turtle")
async def connect(websocket: WebSocket):
    await websocket.accept()
    connected_clients.add(websocket)

    try:
        while True:
            message = await websocket.receive_text()
            # WebSocket receive loop, though we expect the server to send commands, not receive them.
    except WebSocketDisconnect:
        connected_clients.remove(websocket)


@app.post("/movement")
async def movement(movement: Movement):
    for client in connected_clients:
        await client.send_text(movement.json())
    return {"message": "Command sent to all connected clients"}


@app.get("/")
async def home():
    return RedirectResponse(url="/docs", status_code=status.HTTP_302_FOUND)
