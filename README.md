# Rover Control

A user-friendly platform enabling easy control of rovers in both simulated and real-world settings. It offers a visually intuitive programming interface for defining rover movements and logic, adaptable to run in simulations (Gazebo with ROS) and real rovers via Space Packets communication.

## Installation

Clone the repository and then install via pip:

```
cd python-rover-control
python -m venv venv
source venv/bin/activate
pip install -e .
```

## Getting Started with Simulation

## 1. 2D Simulation

### 1. Start the Simulation Server:

Open a terminal or command prompt and run:

```
server_sim
```

### 2. Start the Controller:

Open another terminal or command prompt and run:

```
controller_a
```

### 3. Start the 2D Viewer:

Open another terminal or command prompt and run:

```
viewer2d_a
```

## 2. 3D Simulation

### 1. Start the Webots Server:

- Navigate to the src\webot_server_sim\worlds folder.
- Open the my_first_simulation.wbt file to launch the Webots server.

### 2. Run the Client:
Open a terminal or command prompt and run:

```
python client.py
```


## Contribute

- Issue Tracker: https://gitlab.com/librecube/prototypes/xxx/-/issues
- Source Code: https://gitlab.com/librecube/prototypes/xxx

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube documentation](https://librecube.gitlab.io/).

## Support

If you are having issues, please let us know. Reach us at
[Matrix](https://app.element.io/#/room/#librecube.org:matrix.org)
or via [Email](mailto:info@librecube.org).

## License

The project is licensed under the MIT license. See the [LICENSE](./LICENSE.txt) file for details.
